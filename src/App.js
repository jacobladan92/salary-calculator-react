import React, { Component } from 'react';
import { Tile } from './Containers/tile';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Tile />
      </div>
    );
  }
}

export default App;
